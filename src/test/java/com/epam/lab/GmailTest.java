package com.epam.lab;

import com.epam.lab.bo.ImportantMessageBO;
import com.epam.lab.bo.InboxBO;
import com.epam.lab.bo.LoginBO;
import com.epam.lab.bo.NavigateBO;
import com.epam.lab.listener.TestListener;
import com.epam.lab.utils.DriverManager;
import com.epam.lab.utils.SavedContext;
import io.qameta.allure.Description;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

@Listeners({TestListener.class})
public class GmailTest {

    @DataProvider(parallel = true)
    public Object[][] users() {
        return new Object[][]{
                new Object[]{"sofii.test@gmail.com", "QwErTyUi"},
                new Object[]{"sofii.test2@gmail.com", "QwErTyUi"},
                new Object[]{"sofiiatest3@gmail.com", "QwErTyUi"},
                new Object[]{"sofii.test4@gmail.com", "QwErTyUi"},
                new Object[]{"sofii.test5@gmail.com", "QwErTyUi"}
        };
    }

    @Test(dataProvider = "users", description = "Valid login, mark important messages, delete this massages")
    @Description("Valid login to Gmail, marking messages as important, deleting this messages")
    public void loginMarkImportantAndDeleteMessage(String email, String password) {
        NavigateBO navigateBO = new NavigateBO();
        navigateBO.loadBasePage();
        LoginBO loginBO = new LoginBO();
        loginBO.login(email, password);

        InboxBO inboxBO = new InboxBO();
        inboxBO.markMessagesAsImportant(3);
        inboxBO.navigateToImportantFolder();
        navigateBO.refreshPage();

        ImportantMessageBO importantMessageBO = new ImportantMessageBO();
        assertTrue(importantMessageBO.areMessagesMovedToImportant());
        importantMessageBO.deleteImportantMessages();
        assertTrue(importantMessageBO.areMessagesDeletedFromImportant());
    }

    @AfterMethod
    public void after() {
        SavedContext.clearSavedContext();
        DriverManager.close();
    }
}
