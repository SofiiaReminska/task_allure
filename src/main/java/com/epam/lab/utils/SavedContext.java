package com.epam.lab.utils;

import java.util.HashMap;
import java.util.Map;

public class SavedContext {
    public static final String SAVED_MESSAGE_IDS = "savedMessageIds";
    private static ThreadLocal<Map<String, Object>> savedContext = new ThreadLocal<>();

    public static Map<String, Object> getSavedContext() {
        if (savedContext.get() == null)
            savedContext.set(new HashMap<>());
        return savedContext.get();
    }

    public static void clearSavedContext() {
        if (savedContext.get() != null) {
            savedContext.remove();
        }
    }

    @Override
    public String toString() {
        return savedContext.get().toString();
    }
}
