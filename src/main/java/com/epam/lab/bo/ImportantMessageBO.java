package com.epam.lab.bo;

import com.epam.lab.po.ImportantMessagePage;
import com.epam.lab.utils.DriverManager;
import com.epam.lab.utils.SavedContext;
import com.epam.lab.utils.WaitUtil;

import java.util.List;

public class ImportantMessageBO {

    private ImportantMessagePage importantMessagePage = new ImportantMessagePage(DriverManager.getDriver());

    @SuppressWarnings("unchecked")
    public boolean areMessagesMovedToImportant() {
        final List<String> expected = (List<String>) SavedContext.getSavedContext().get(SavedContext.SAVED_MESSAGE_IDS);
        return importantMessagePage.getMailIds().containsAll(expected);
    }

    public boolean areMessagesDeletedFromImportant() {
        return importantMessagePage.isNoImportantMessageLabelVisible();
    }

    public void deleteImportantMessages() {
        importantMessagePage.selectAllImportantMessages();
        importantMessagePage.deleteCheckedMessages();
        WaitUtil.waitConstTime(3);
    }
}
