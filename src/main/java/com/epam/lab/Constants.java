package com.epam.lab;

import org.openqa.selenium.By;

public class Constants {
    public static final int DEFAULT_TIMEOUT = 15;

    public static final By MESSAGE_ID_XPATH = By.xpath(".//ancestor::tr[@jscontroller]//span[@data-legacy-thread-id]");
    public static final String MESSAGE_ID_ATTRIBUTE = "data-legacy-thread-id";
}
