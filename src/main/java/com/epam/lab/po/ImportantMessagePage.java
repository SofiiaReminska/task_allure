package com.epam.lab.po;

import com.epam.lab.Constants;
import com.epam.lab.utils.WaitUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.stream.Collectors;

public class ImportantMessagePage extends BasePage {
    private static final Logger LOG = LogManager.getLogger(ImportantMessagePage.class);

    @FindBy(xpath = "//tr[@jsaction]//div[@role='checkbox']")
    private List<WebElement> checkBoxes;

    @FindBy(xpath = "//tr[@jsaction]//div[@role='link']//span[@data-legacy-thread-id]")
    private List<WebElement> mailSubjects;

    @FindBy(xpath = "//*[@role='checkbox']//div[@role='presentation']/parent::*")
    private WebElement selectAllMessagesCheckbox;

    @FindBy(xpath = "//*[@act='10']")
    private WebElement deleteMessagesButton;

    @FindBy(xpath = "//*[@role='main']//*[@style='text-align:center']")
    private WebElement noImportantMessagesLabel;

    public ImportantMessagePage(WebDriver driver) {
        super(driver);
    }

    public List<String> getMailIds() {
        return mailSubjects.stream().map(el -> el.getAttribute(Constants.MESSAGE_ID_ATTRIBUTE)).collect(Collectors.toList());
    }

    public void selectAllImportantMessages() {
        LOG.info("Selecting all important messages");
        selectAllMessagesCheckbox.click();
    }

    public void deleteCheckedMessages() {
        LOG.info("Deleting checked messages");
        WaitUtil.waitForVisibleAndClickable(deleteMessagesButton);
        deleteMessagesButton.click();
        WaitUtil.waitForInvisible(deleteMessagesButton);
    }

    public boolean isNoImportantMessageLabelVisible() {
        LOG.info("Checking if message label is displayed ");
        return noImportantMessagesLabel.isDisplayed();
    }
}
